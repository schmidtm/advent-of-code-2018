var DayAdvent = {
	performPart1: function(output, currentValue)
	{
		currentValue = currentValue.split(",");
		currentValue = currentValue.map(val => parseInt(val.replace(/[^\d\-]+/, ""), 10) );
		
		currentValue = currentValue.reduce((accum, currentValue) => accum + currentValue, 0);
		var res = {"itm":currentValue};

		output.push(JSON.stringify(res));
			
			
			
			
		return output;
	},
	performPart2: function(output, currentValue)
	{
		
		currentValue = currentValue.split(",");
		currentValue = currentValue.map(val => parseInt(val.replace(/[^\d\-]+/, ""), 10) );
			
		var freqList = {};
		var curFreq = 0;
		var cnt = 0; 
		while (!  freqList[curFreq]) {
			freqList[curFreq] = true;
			curFreq = currentValue[cnt] + curFreq;

			cnt++;
			if ( cnt >= currentValue.length)
				cnt = 0;
		}


		var res = {"itm":curFreq};
		output.push(JSON.stringify(res));
		
			
			
		return output;

	}
	
}
try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}
