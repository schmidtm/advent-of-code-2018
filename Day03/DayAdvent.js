var DayAdvent = {
	
	matrix : [],
	Part1: function()
	{
		
		
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
		 outStr = vals.reduce(this.performPart1.bind(this), []);
		 outStr = this.countNumUnique(this.matrix);
		this.WriteOutput(outStr);
	
	},
	countNumUnique: function(arr) {
		var res = arr.reduce(function(prevVal, curVal) { 
			if ( prevVal == undefined) 
				prevVal = 0;
			return prevVal + curVal.reduce(
				function(prevVal, curVal) {
				
					if ( curVal == "x" )
						prevVal++;
					return prevVal;
				}, 0
			);

		}, 0);
		
		return res;
	},
	expandMultiArrayTo: function(arr, width, height)
	{
		var diffHeight =  (height) - arr.length;
		var curWidth = arr.length == 0 ? 0 : arr[0].length;
		var newWidth = Math.max(curWidth, width);
		

		/* expand Width */
		if ( curWidth < newWidth) {
			for ( var i = 0; i < arr.length; i++) {
				arr[i].length = newWidth;
				arr[i].fill(".", curWidth, newWidth);
			}
		}

		/* Expand height */

		for ( var i = arr.length; i < diffHeight; i++) {
			var tmpArr = [];
			tmpArr.length = newWidth;
			arr.push(tmpArr.fill(".", 0, newWidth));
		}

		return arr;
	},
	addItemsToArr: function(arr, identifier, leftOffset, topOffset, width, height) {

		for ( var curHeight = 0; curHeight < height; curHeight++) {
			var ht = curHeight + topOffset-1;
			for ( var curWidth = 0; curWidth < width; curWidth++)
			{
				var wd = curWidth + leftOffset-1;
				if ( arr[ht][wd] != ".")
				{
					arr[ht][wd] = "x";
				} else {
					arr[ht][wd] = identifier;
				}
				
			}
		}	
	},
	parseInput: function(inp) {
		var inpRegExp = /\#(\d+) \@ (\d+)\,(\d+)\: (\d+)x(\d+)/gi;
		var res = inpRegExp.exec(inp);
		return {
			identifier: res[1],
			leftOffset: parseInt(res[2], 10),
			topOffset: parseInt(res[3], 10),
			width: parseInt(res[4], 10),
			height: parseInt(res[5], 10)
		}
	},
	performPart1: function(output, currentValue)
	{
		var res = {"itm":currentValue};
		//output.push(JSON.stringify(res));
		
		var input = this.parseInput(currentValue);
		output.push(JSON.stringify(input));
		var arr = this.matrix;
		this.expandMultiArrayTo(arr, input.width+input.leftOffset,input.height+input.topOffset);
		this.addItemsToArr(arr, input.identifier, input.leftOffset, input.topOffset, input.width, input.height);

	
		
		return output;
	},
	performPart2: function(output, currentValue)
	{
		var res = {"itm":currentValue};
		output.push(JSON.stringify(res));
			
			
			
			
		return output;

	}
	
}
try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}
