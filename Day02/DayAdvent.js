var DayAdvent = {
	performPart1: function(output, currentValue)
	{
		var vals = currentValue.split(",")
			
		var twoCount = 0;
		var threeCount = 0;
		
		
		vals.forEach(i => {
			var isTwoCount = false;
			var isThreeCount = false;
			var curLen = i.length;
			console.log(i);
			while ( curLen > 0 && ( ! isTwoCount || !isThreeCount) )
			{
				i = i.replace(new RegExp(i[0], "g"), "");
				var dif = curLen - i.length;
				console.log(dif);
				if ( dif == 2)
					isTwoCount = true;
				else if ( dif  == 3) 
					isThreeCount = true;
				curLen = i.length;
			}
			if (isTwoCount) 
				twoCount++;
			if ( isThreeCount)
				threeCount++;
		})
		console.log(twoCount, threeCount);
		var res = {"itm":twoCount* threeCount};
		output.push(JSON.stringify(res));
			
		return output;
	},
	performPart2: function(output, currentValue)
	{
		var matchNdxs = [];
		var vals = currentValue.split(",")
		vals.some((i,ndx, arr) => {
			if ( ! arr.some(d => {if ( d === i ) 
				{ 
					return false; 
			} else if ( DayAdvent.closeByOne(d, i) ) {
				matchNdxs.push(d);
				 return true; 
			} else { 
				return false; 
			} }  ) )
				return false;
			else
			{
				matchNdxs.push(i);
				return true;
			}
		} )
			
		
			
		return matchNdxs;

	},
	closeByOne(str1, str2) {
		if ( str1.length != str2.length )
			return false;
		var cntDiff = 0;
		for ( var i = 0; i < str1.length; i++)
		{
			if ( str1[i] != str2[i] )
				cntDiff++;
			if ( cntDiff > 1 )
				return false;	
		}
		if ( cntDiff == 0)
			return false;
		return true;
	}
	
}
try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}
